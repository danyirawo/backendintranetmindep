<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Funcionarios\FuncionarioController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//Listado de movies enviando la lógica en la ruta
/* Route::get('movies', function(){
    return Movie::all();
}); */

//listado de funcionarios enviado la lógica en controlador
Route::get('funcionario', [FuncionarioController::class, 'index']);

//obteniendo un funcionario por id
Route::get('funcionario/{token}', [FuncionarioController::class, 'show']);

//listado de funcionarios eque cumplen en el dia
Route::get('cumple', [FuncionarioController::class, 'cumplefuncionarios']);

//guardar o crear un dato dentro de la BD usando un api
//Route::post('funcionario', [FuncionarioController::class, 'store']);

//actualiza un dato existente de la BD usando un api
//Route::put('mfuncionarioovies/{id}', [FuncionarioController::class, 'update']);

//eliminar un dato existente de la BD usando un api
//Route::delete('funcionario/{id}', [FuncionarioController::class, 'delete']);

<?php

namespace App\Http\Controllers\Funcionarios;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PyR\Userintranet; 
use App\Models\PyR\PyR;
use App\Models\PyR\Vacaciones;
use App\Models\PyR\PermisosAdministrativos;
use App\Models\User;
use Carbon\Carbon;

class FuncionarioController extends Controller
{
    public function index(){
        return PyR::all();
    }

    public function show($token){

        $emailFuncionario = Userintranet::where('api_token', $token)->first();
        $datosFuncionarioPyR = PyR::where('email', $emailFuncionario->email)->first();
        $datosFuncionarioWSAD = User::where('email', $emailFuncionario->email)->first();
        $datosFuncionarioVacaciones = Vacaciones::where('CodigoPersona', $datosFuncionarioPyR->codigo_persona)->orderby('id' , 'desc')->first();
        $datosFuncionariosAdminsitrativos = PermisosAdministrativos::Where('CodigoPersona', $datosFuncionarioPyR->codigo_persona)->where('TipoPermiso', 'Con Goce')->orderby('id' , 'desc')->first();       

        return [
            'datosFuncionarioPyR' => $datosFuncionarioPyR,
            'datosFuncionarioVacaciones' => $datosFuncionarioVacaciones,
            'datosFuncionarioWSAD' => $datosFuncionarioWSAD,
            'datosFuncionariosAdminsitrativos' => $datosFuncionariosAdminsitrativos
        ];
    }


    public function cumplefuncionarios(){

        $nombresCumpleaneros = [];
        $month = Carbon::now()->format('m');
        $day = Carbon::now()->format('d');
        
        $fechasCumple = PyR::whereMonth('fecha_nacimiento', $month)->whereDay('fecha_nacimiento', $day)->whereNotNull('email')->get();

        if($fechasCumple){
            foreach ($fechasCumple as $key => $value) {
                $nombreCumpleanero = $value->nombres . ' ' . $value->apellido_paterno . ' ' . $value->apellido_materno;
                $nombresCumpleaneros[] = $nombreCumpleanero;
            }
        }

        return response()->json($nombresCumpleaneros);

    }
}

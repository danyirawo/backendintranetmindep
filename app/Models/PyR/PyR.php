<?php

namespace App\Models\PyR;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\PyR\Userintranet;


class PyR extends Model
{

        protected $connection = 'mysqlpyr';
		protected $table =  'personas';
        protected $fillable = ['id','periodo','mes', 'codigo_persona','nombres','apellido_paterno','apellido_materno','sexo', 'fecha_nacimiento', 'edad', 'estado_civil', 'email', 'profesion', 'nacionalidad', 'activo', 'funcion', 'cargo', 'planta', 'tipo_funcionario', 'calidad_juridica', 'grado', 'unidad_desempeno', 'id_grupo', 'codigo_juego'];
        public $timestamps = false;


	public function getNombrescAttribute()
    {

       return $this->apellido_paterno.' '.$this->nombres.' '.' - '.$this->email;

    }

    public function getNameAttribute()
    {

        return $this->nombres.' '.$this->apellido_paterno.' '.$this->apellido_materno.' '.'  -  '.$this->unidad_desempeno;

    }

    public function User()
	{
		$relacion = $this->belongsTo(User::class, 'email', 'email');
		if($relacion){
			$relacion = $this->belongsTo(User::class, 'email', 'email');
		}else{
			$relacion = null;
		}

		return $relacion;
	}

    public function Userintranet()
	{
		$relacion = $this->belongsTo(Userintranet::class, 'email', 'email');
		if($relacion){
			$relacion = $this->belongsTo(Userintranet::class, 'email', 'email');
		}else{
			$relacion = null;
		}

		return $relacion;
	}


    
}
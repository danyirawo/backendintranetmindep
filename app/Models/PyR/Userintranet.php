<?php

namespace App\Models\PyR;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\PyR\PyR;


class Userintranet extends Model
{

        protected $connection = 'mysqlpyr';
		protected $table =  'userintranet';
        protected $fillable = ['id', 'api_token' , 'email', 'password', 'deleted_at'];
        public $timestamps = false;


    public function User()
	{
		$relacion = $this->belongsTo(User::class, 'email', 'email');
		if($relacion){
			$relacion = $this->belongsTo(User::class, 'email', 'email');
		}else{
			$relacion = null;
		}

		return $relacion;
	}

    public function PyR()
	{
		$relacion = $this->belongsTo(PyR::class, 'email', 'email');
		if($relacion){
			$relacion = $this->belongsTo(PyR::class, 'email', 'email');
		}else{
			$relacion = null;
		}

		return $relacion;
	}


    
}
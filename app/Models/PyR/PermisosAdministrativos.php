<?php

namespace App\Models\PyR;

use Illuminate\Database\Eloquent\Model;
use App\PyR\Userintranet;
use App\PyR\PyR;

class PermisosAdministrativos extends Model
{
        protected $connection = 'mysqlpyr';
		protected $table =  'permisosadministrativos';
        protected $fillable = ['eMail','Activo','Ano','Mes','CodigoPersona','NumResolucion','NumDecreto','Causa',
                                'FechaInicio','FechaTermino','CantidadPermiso','Observaciones','TipoPermiso','DiaHora',
                                'AnoProceso','MesProceso','Jornada','Saldo','Anulado'];
        public $timestamps = false;

        public function PyR()
	{
		$relacion = $this->belongsTo(PyR::class, 'codigo_persona', 'CodigoPersona');
		if($relacion){
			$relacion = $this->belongsTo(PyR::class, 'codigo_persona', 'CodigoPersona');
		}else{
			$relacion = null;
		}

		return $relacion;
	}

    public function Userintranet()
	{
		$relacion = $this->belongsTo(Userintranet::class, 'email', 'eMail');
		if($relacion){
			$relacion = $this->belongsTo(Userintranet::class, 'email', 'eMail');
		}else{
			$relacion = null;
		}

		return $relacion;
	}
}

<?php

namespace App\Models\PyR;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\PyR\Userintranet;
use App\PyR\PyR;

class Vacaciones extends Model
{
        protected $connection = 'mysqlpyr';
		protected $table =  'vacaciones';
        protected $fillable = ['id','Nombres', 'Apellidos', 'Direccion', 'FechaNacimiento', 'EstadoCivil', 'Escolaridad', 'Profesion',
                                'Fono', 'eMail', 'Funcion', 'Cargo', 'CodigoPlanta', 'Grado', 'CalidadJuridica', 'CentroCosto', 'Genero',
                                'FechaIngreso', 'Nombre_Jefe', 'Apellidos_Jefe', 'Correo_Jefe', 'UnidadDependecia', 'UnidadDesempeno', 'Region',
                                'Comuna', 'Activo', 'Ano', 'Mes', 'CodigoPersona', 'NumResolucion', 'FechaInicio', 'FechaTermino', 'NumDiasSolicitados',
                                'NumDiasOtorgados', 'DiasPendientes'];
        public $timestamps = false;


        public function User()
	{
		$relacion = $this->belongsTo(User::class, 'email', 'eMail');
		if($relacion){
			$relacion = $this->belongsTo(User::class, 'email', 'eMail');
		}else{
			$relacion = null;
		}

		return $relacion;
	}

    public function Userintranet()
	{
		$relacion = $this->belongsTo(Userintranet::class, 'email', 'eMail');
		if($relacion){
			$relacion = $this->belongsTo(Userintranet::class, 'email', 'eMail');
		}else{
			$relacion = null;
		}

		return $relacion;
	}

    public function PyR()
	{
		$relacion = $this->belongsTo(PyR::class, 'codigo_persona', 'CodigoPersona');
		if($relacion){
			$relacion = $this->belongsTo(PyR::class, 'codigo_persona', 'CodigoPersona');
		}else{
			$relacion = null;
		}

		return $relacion;
	}
}